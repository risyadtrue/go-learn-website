package go_learn_web

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func RequestHeader(writer http.ResponseWriter, request *http.Request) {
	contentType := request.Header.Get("content-type")
	fmt.Fprint(writer, contentType)
}

func ResponseHeader(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Add("X-Powered-By", "Ripang Ketapang")
	fmt.Fprint(writer, "OK")
}

func TestRequestHeader(t *testing.T) {
	// bikin request
	request := httptest.NewRequest(http.MethodGet, "http://locahost:8000/", nil)
	request.Header.Add("Content-type", "application/json")

	// bikin writer pake recorder
	recorder := httptest.NewRecorder()

	RequestHeader(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)

	fmt.Println(string(body))
}

func TestResponseHeader(t *testing.T) {
	// bikin request
	request := httptest.NewRequest(http.MethodGet, "http://locahost:8000/", nil)
	request.Header.Add("Content-type", "application/json")

	// bikin writer pake recorder
	recorder := httptest.NewRecorder()

	ResponseHeader(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)

	fmt.Println(string(body))
	fmt.Println(response.Header.Get("x-powered-by"))

}
