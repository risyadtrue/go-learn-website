package go_learn_web

import (
	"fmt"
	"net/http"
	"testing"
)

type LogMiddleware struct {
	Handler http.Handler
}

func (middleware *LogMiddleware) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	fmt.Println("before execure handler")
	// logic pengecekan data
	// dia nerusin ke handler setelahnya
	middleware.Handler.ServeHTTP(writer, request)
	fmt.Println("after execure handler")
}

type ErrorHandler struct {
	Handler http.Handler
}

func (errorHandler *ErrorHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	// middleware for error function
	defer func() {
		err := recover()
		if err != nil {
			fmt.Println("error euuy")
			writer.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(writer, "error: %s", err)
		}
	}()
	errorHandler.Handler.ServeHTTP(writer, request)

}

func TestMiddleware(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Println("Handler Execured")
		fmt.Fprint(writer, "Hello middleware")
	})
	mux.HandleFunc("/panic", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Println("panic Execured")
		panic("Ups")
	})

	// mid kedua ke mux
	logMiddleware := &LogMiddleware{
		Handler: mux,
	}

	// mid pertama ke log
	errorHandler := &ErrorHandler{
		Handler: logMiddleware,
	}

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: errorHandler,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}

}
