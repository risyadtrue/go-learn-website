package go_learn_web

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func FormPost(writer http.ResponseWriter, request *http.Request) {
	err := request.ParseForm()
	if err != nil {
		panic(err)
	}

	// bisa juga tanpa deklarasi untuk parse
	// name := request.PostFormValue("first_name")

	firstname := request.PostForm.Get("first_name")
	lastname := request.PostForm.Get("last_name")

	fmt.Fprintf(writer, "Hello %s %s", firstname, lastname)

}

func TestFormPost(t *testing.T) {
	requestBody := strings.NewReader("first_name=ripang&last_name=katapang")
	request := httptest.NewRequest(http.MethodPost, "http://localhost:8080", requestBody)
	request.Header.Add("Content-type", "application/x-www-form-urlencoded") // wajib bangett!!
	recorder := httptest.NewRecorder()

	FormPost(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	fmt.Println(string(body))
}
