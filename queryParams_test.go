package go_learn_web

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

// handler
func SayHello(writer http.ResponseWriter, request *http.Request) {
	fmt.Println(request.URL)
	name := request.URL.Query().Get("name")
	if name == "" {
		fmt.Fprint(writer, "Hello")
	} else {
		fmt.Fprintf(writer, "Hello %s", name)
	}
}

func TestQueryParams(t *testing.T) {
	// bikin request
	request := httptest.NewRequest(http.MethodGet, "http://locahost:8000/hello?name=ripang", nil)
	// bikin writer pake recorder
	recorder := httptest.NewRecorder()

	SayHello(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	fmt.Println(string(body))
}

func MultipleQueryParams(writer http.ResponseWriter, request *http.Request) {
	firstName := request.URL.Query().Get("firstName")
	lastName := request.URL.Query().Get("lastName")
	fmt.Fprintf(writer, "Hello %s %s", firstName, lastName)
}

func TestMultipleQueryParameters(t *testing.T) {
	// bikin request
	request := httptest.NewRequest(http.MethodGet, "http://locahost:8000/hello?firstName=ripang&lastName=ketapang", nil)
	// bikin writer pake recorder
	recorder := httptest.NewRecorder()

	MultipleQueryParams(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	fmt.Println(string(body))
}

func MultipleQueryParamsValues(writer http.ResponseWriter, request *http.Request) {
	query := request.URL.Query()
	name := query["name"]
	fmt.Fprint(writer, strings.Join(name, " "))
}

func TestMultipleQueryParameterValues(t *testing.T) {
	// bikin request
	request := httptest.NewRequest(http.MethodGet, "http://locahost:8000/hello?name=ripang&name=ketapang", nil)
	// bikin writer pake recorder
	recorder := httptest.NewRecorder()

	MultipleQueryParamsValues(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	fmt.Println(string(body))
}
